package de.ragecode.lobby.main;

import de.ragecode.lobby.methods.Methods;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;

import de.ragecode.lobby.mysql.MySQL;


public class Lobby extends JavaPlugin {

    private static Lobby instance;
    public static MySQL mysql;

    @Override
    public void onEnable() {
        //OnEnable Messages
        Bukkit.getConsoleSender().sendMessage("§7§m---------------------------------------------------------");
        Bukkit.getConsoleSender().sendMessage("§c§lShortException LobbySystem | 1.0 BETA SNAPSHOT");
        Bukkit.getConsoleSender().sendMessage("§c§lDieses System wurde von ShortException Programmiert");
        Bukkit.getConsoleSender().sendMessage("§c§lSie haben keine Rechte um dieses System zu Decompilen!");
        Bukkit.getConsoleSender().sendMessage("§c§lBitte warten sie das System wird zur Datenbank verbunden");
        Bukkit.getConsoleSender().sendMessage("§7§m---------------------------------------------------------");
        

        //MySQL Utils   
        mysql = new MySQL("localhost", "root", "ragecode1012", "Lobby");
        mysql.connect();
        mysql.createTable("Teamspeak", "NAME VARCHAR(100), UUID VARCHAR(100), ID VARCHAR(100), RANG INTEGER, TYPE INTEGER");

        //Utils
        Methods.registerall();

     
    }

    @Override
    public void onDisable() {
        System.out.println("Plugin wurde Deaktiviert!");
    }

    public static Lobby getInstance() {
        return instance;
    }
}

package de.ragecode.lobby.extras;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

import de.ragecode.lobby.methods.Prefix;

public class UnknownCommandEvent implements Listener {

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        if (!e.isCancelled()) {
            Player p = e.getPlayer();
            String msg = e.getMessage().split(" ")[0];
            HelpTopic help = Bukkit.getServer().getHelpMap().getHelpTopic(msg);

            if (help == null) {
                e.setCancelled(true);
                p.sendMessage(Prefix.pr + "§eDieser Befehl wurde nicht im System gefunden.");
            }
        }
    }

}

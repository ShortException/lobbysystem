package de.ragecode.lobby.extras;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.ragecode.lobby.methods.JoinUtils;
import org.bukkit.event.EventHandler;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        JoinUtils.removeItems(p);
        e.setQuitMessage(null);
    }

}

package de.ragecode.lobby.extras;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import de.ragecode.lobby.methods.JoinUtils;
import net.ragecode.tryapi.API.TitleAPI;

public class JoinListener implements Listener {

    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        Player p = e.getPlayer();
        if (!p.hasPlayedBefore()) {
            p.getInventory().clear();
            JoinUtils.setItems(p);
            //TrySoulDE
            TitleAPI.sendActionBar(p, "§eHerzlich Willkommen §7" + p.getDisplayName() + "§eauf §bT§5r§ay§9S§5o§6u§1l§8§lD§r§c§lDE§r");
            Firework fw = (Firework) p.getWorld().spawn(p.getLocation(), Firework.class);
            FireworkMeta fwm = fw.getFireworkMeta();
            FireworkEffect eff = FireworkEffect.builder().flicker(true).withColor(Color.RED).withColor(Color.RED).withColor(Color.RED).withColor(Color.RED).withColor(Color.RED).withColor(Color.ORANGE).withColor(Color.YELLOW).withColor(Color.GREEN).withColor(Color.AQUA).withFade(Color.BLUE).with(Type.BALL_LARGE).trail(true).build();
            fwm.addEffect(eff);
            fw.setFireworkMeta(fwm);
                    
        } else if (p.hasPlayedBefore()) {
            p.getInventory().clear();
            JoinUtils.setItems(p);
            TitleAPI.sendActionBar(p, "§eWillkommen Zurück §7" + p.getDisplayName() + "§e auf TrySoulDE");

        } else {

        }
    }
}

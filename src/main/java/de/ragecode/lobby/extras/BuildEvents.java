package de.ragecode.lobby.extras;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import de.ragecode.lobby.methods.Arrays;

public class BuildEvents implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {

        Player p = e.getPlayer();

        if (Arrays.builden.contains(p)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {

        Player p = e.getPlayer();
        if (Arrays.builden.contains(p)) {
            e.setCancelled(false);
            e.setBuild(true);
        } else {
            e.setCancelled(true);
            e.setBuild(false);
        }
    }

    @EventHandler
    public void onPlace(BlockDamageEvent e) {

        Player p = e.getPlayer();

        if (Arrays.builden.contains(p)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBurn(BlockBurnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockFrom(BlockFormEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onLeave(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {

        Player p = e.getPlayer();

        if (Arrays.builden.contains(p)) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {

        Player p = e.getPlayer();
        if (Arrays.builden.contains(p)) {
            e.setCancelled(false);
        } else {
            e.getItemDrop().remove();
            p.setItemInHand(e.getItemDrop().getItemStack());
            p.updateInventory();
        }
    }

}

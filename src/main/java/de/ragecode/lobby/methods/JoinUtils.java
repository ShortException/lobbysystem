package de.ragecode.lobby.methods;

import net.ragecode.tryapi.API.ItemBuilder;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class JoinUtils {

    public static void setItems(Player p) {
        if (p.hasPermission("Lobby.Admin")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §4Administrator §7•§6§l Boots", Color.RED, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(2, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §eReports"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.TNT, 0, 0, "§7→ §c§lSilentHub"));
            p.getInventory().setItem(5, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.Dev")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §bEntwickler §7•§6§l Boots", Color.AQUA, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.TNT, 0, 0, "§7→ §c§lSilentHub"));
            p.getInventory().setItem(5, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));

        } else if (p.hasPermission("Lobby.SrMod")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §cSrModerator §7•§6§l Boots", Color.RED, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.TNT, 0, 0, "§7→ §c§lSilentHub"));
            p.getInventory().setItem(5, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.Mod")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §cModerator §7•§6§l Boots", Color.RED, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.TNT, 0, 0, "§7→ §c§lSilentHub"));
            p.getInventory().setItem(5, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.Supp")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §9Supporter §7•§6§l Boots", Color.BLUE, ""));

        } else if (p.hasPermission("Lobby.Ytber")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §5Youtuber §7•§6§l Boots", Color.PURPLE, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.TNT, 0, 0, "§7→ §c§lSilentHub"));
            p.getInventory().setItem(5, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.Premium")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §6Premium §7•§6§l Boots", Color.ORANGE, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(3, ItemBuilder.createItem(Material.EMERALD, 0, 0, "§7→ §9§lSchutzSchild"));
            p.getInventory().setItem(6, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.User")) {
            p.getInventory().setBoots(ItemBuilder.createLeatherBoots("§7→ §8Spieler §7•§6§l Boots", Color.GRAY, ""));
            p.getInventory().setItem(0, ItemBuilder.createItem(Material.COMPASS, 0, 0, "§7→ §e§lNavigator"));
            p.getInventory().setItem(1, ItemBuilder.createItem(Material.BLAZE_ROD, 0, 0, "§7→ §6Spieler Verstecken"));
            p.getInventory().setItem(4, ItemBuilder.createItem(Material.COMMAND, 0, 0, "§7→ §9§lEinstellungen"));
            p.getInventory().setItem(7, ItemBuilder.createItem(Material.NETHER_STAR, 0, 0, "§7→ §b§lLobby Selector"));
            p.getInventory().setItem(8, ItemBuilder.createHeadItem(p.getName(), "§7→ §6§lDein Profil"));
        } else if (p.hasPermission("Lobby.Team")) {
            p.getInventory().setChestplate(ItemBuilder.createLeatherChest("§7→ §eDein Besonderer §7• §6§lTeam BrustPanzer", Color.SILVER, "§7Vielen Danke dass du im Team bist §4§l❤"));

        } else if (p.hasPermission("Lobby.All")) {
            p.setGameMode(GameMode.ADVENTURE);
            p.setFireTicks(0);
            p.setExp(0);
            p.setLevel(2017);
            p.setFoodLevel(20);
            p.setHealth(20.0);

            p.playSound(p.getLocation(), Sound.LEVEL_UP, 3, 0);
        }
    }

    public static void removeItems(Player p) {
        p.getInventory().clear();

    }
}

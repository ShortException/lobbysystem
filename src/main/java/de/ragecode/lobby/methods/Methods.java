package de.ragecode.lobby.methods;

import de.ragecode.lobby.commands.BuildCommand;
import de.ragecode.lobby.commands.VerifyCommand;
import de.ragecode.lobby.extras.BuildEvents;
import de.ragecode.lobby.extras.JoinListener;
import de.ragecode.lobby.extras.QuitListener;
import de.ragecode.lobby.extras.UnknownCommandEvent;
import de.ragecode.lobby.extras.WorldEvents;
import de.ragecode.lobby.main.Lobby;

public class Methods {

    public static void registerEvents() {
        Lobby.getInstance().getServer().getPluginManager().registerEvents(new BuildEvents(), Lobby.getInstance());
        Lobby.getInstance().getServer().getPluginManager().registerEvents(new UnknownCommandEvent(), Lobby.getInstance());
        Lobby.getInstance().getServer().getPluginManager().registerEvents(new JoinListener(), Lobby.getInstance());
        Lobby.getInstance().getServer().getPluginManager().registerEvents(new WorldEvents(), Lobby.getInstance());
        Lobby.getInstance().getServer().getPluginManager().registerEvents(new QuitListener(), Lobby.getInstance());
    }

    public static void registerCommands() {
        Lobby.getInstance().getCommand("build").setExecutor(new BuildCommand());
        Lobby.getInstance().getCommand("verify").setExecutor(new VerifyCommand());
        Lobby.getInstance().getCommand("unlink").setExecutor(new VerifyCommand());
    }

    public static void registerall() {
        registerEvents();
        registerCommands();
    }
}

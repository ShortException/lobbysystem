package de.ragecode.lobby.methods;

public class Prefix {
	
	public static String pr = "§6§lTrySoul §r§7|§r ";
	public static String noperm = pr + "§c§lDu hast keine Rechte um diesen Befehl auszuführen zu dürfen.";
	public static String fehler = pr + "§c§lEs gab einen Fehler im System siehe in der Konsole nach.";
	public static String noplayer = pr + "§cDu musst einen Spieler sein um diesen Befehl auszuführen zu können.";
	
}

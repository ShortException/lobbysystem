package de.ragecode.lobby.inventory;

import net.ragecode.tryapi.API.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class NavigatorInteract implements Listener {

    @EventHandler
    public void on(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            try {
                if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aNavigator")) {
                    Inventory inv = Bukkit.createInventory(null, 54, "§aNavigation");
                    p.openInventory(inv);

                }
            } catch (Exception ex) {
            }
        }
    }

    @EventHandler
    public void on(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getInventory().getName().equalsIgnoreCase("�aNavigation")) {
            try {
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSpawn")) {
                    p.teleport(new Location(p.getWorld(), -221, 98, -198));
                    e.getView().close();
                }
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSpiel 1")) {
                    p.teleport(new Location(p.getWorld(), -252, 80, -182));
                    e.getView().close();
                }
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSpiel 2")) {
                    p.teleport(new Location(p.getWorld(), -221, 82, -162));
                    e.getView().close();
                }
            } catch (Exception ex) {
            }
        }
    }
}

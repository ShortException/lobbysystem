package de.ragecode.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ragecode.lobby.methods.Prefix;
import de.ragecode.lobby.verify.TSMySQL;

public class VerifyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Prefix.noplayer);
            return true;
        }
        Player p = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("verify")) {
            if (args.length == 1) {
                String ID = args[1];
                if (ID.endsWith("=")) {
                    if (!(TSMySQL.playerExist(p.getUniqueId().toString()))) {
                        TSMySQL.createPlayer(p.getName(), p.getUniqueId().toString(), ID,(p.hasPermission("Admin") ? "Admin" : p.hasPermission("Dev") ? "Developer" : p.hasPermission("SrMod") ? "SrMod" : p.hasPermission("Mod") ? "Mod" : p.hasPermission("Supp") ? "Supp" : p.hasPermission("Ytber") ? "Ytber" : p.hasPermission("Premium") ? "Premium" : "Spieler"));
                        p.sendMessage(Prefix.pr + "§aDu hast eine Nachricht im Teamspeak bekommen, bitte bestätige deine Verifizierung.");
                    } else {
                        p.sendMessage(Prefix.pr + "§cDu bist bereits Verifiziert");
                    }
                } else {
                    p.sendMessage(Prefix.pr + "§cDeine ID ist ungültig!");
                }
            } else {
                p.sendMessage(Prefix.pr + "§7Verwende §e/Verify <TeamspeakID>");
            }

        } else {
            p.sendMessage(Prefix.fehler);
        }
        if (cmd.getName().equalsIgnoreCase("unlink")) {
            if (TSMySQL.playerExist(p.getUniqueId().toString())) {
                TSMySQL.DeleteUUID(p.getUniqueId().toString());
                p.sendMessage(Prefix.pr + "§aDeine Verknüpfung wurde Aufgehoben.");
            } else {
                p.sendMessage(Prefix.pr + "§cDu bist nicht Verifiziert!");
            }
        }
        return true;
    }

}

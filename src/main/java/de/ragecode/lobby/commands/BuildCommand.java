package de.ragecode.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ragecode.lobby.methods.JoinUtils;
import de.ragecode.lobby.methods.Prefix;
import de.ragecode.lobby.methods.Arrays;
import org.bukkit.GameMode;

public class BuildCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Prefix.noplayer);
            return true;
        }

        Player p = (Player) sender;

        if (!p.hasPermission("build.use")) {
            p.sendMessage(Prefix.noperm);
            return true;
        }

        if (cmd.getName().equalsIgnoreCase("build")) {
            if (!Arrays.builden.contains(p)) {
                Arrays.builden.add(p);
                p.sendMessage(Prefix.pr + "§3Build: §aon");
                JoinUtils.removeItems(p);
                p.setGameMode(GameMode.CREATIVE);
            } else {
                Arrays.builden.remove(p);
                p.sendMessage(Prefix.pr + "§3Build: §coff");
                JoinUtils.removeItems(p);
                JoinUtils.setItems(p);
                p.setGameMode(GameMode.ADVENTURE);
                p.setFireTicks(0);
                p.setExp(0);
                p.setLevel(2017);
                p.setFoodLevel(20);
                p.setHealth(20.0);
            }
        }

        return true;

    }

}

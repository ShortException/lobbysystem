package de.ragecode.lobby.verify;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.ragecode.lobby.main.Lobby;

public class TSMySQL {

    public static boolean playerExist(String UUID) {
        try {
            ResultSet rs = Lobby.mysql.getResult("SELECT * FROM Teamspeak WHERE UUID= '" + UUID + "'");
            if (rs.next()) {
                return rs.getString("NAME") != null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean NameExist(String Name) {
        try {
            ResultSet rs = Lobby.mysql.getResult("SELECT * FROM Teamspeak WHERE NAME= '" + Name + "'");
            if (rs.next()) {
                return rs.getString("UUID") != null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean UUIDExist(String ID) {
        try {
            ResultSet rs = Lobby.mysql.getResult("SELECT * FROM Teamspeak WHERE ID= '" + ID + "'");
            if (rs.next()) {
                return rs.getString("UUID") != null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void createPlayer(String Name, String uuid, String ID, String rang) {
        if (!(playerExist(uuid))) {
            int Rang = 0;
            if (rang.equalsIgnoreCase("Admin")) {
                Rang = 11370;
            } else if (rang.equalsIgnoreCase("SrDev")) {
                Rang = 10;
           
            } else if(rang.equalsIgnoreCase("Dev")) {
                Rang = 1;
            }else if(rang.equalsIgnoreCase("SrMod")){
                Rang = 19;
            }
            Lobby.mysql.update("INSERT INTO TEAMSPEAK(NAME, UUID, ID, RANG, TYPE) VALUES ('" + Name + "', '" + uuid + "', '" + ID + "', '" + Rang + "', '0')");
        }
    }

    public static void Delete(String Name) {
        Lobby.mysql.update("DELETE FROM TEAMSPEAK WHERE NAME='" + Name + "'");
    }

    public static void DeleteUUID(String UUID) {
        Lobby.mysql.update("DELETE FROM TEAMSPEAK WHERE NAME='" + UUID + "'");
    }

    public static void updateRang(String UUID, String Rang) {
        int ID = 0;
        if (Rang.equalsIgnoreCase("Admin")) {
            ID = 11370;
        } else {
            ID = 10;

        }
        Lobby.mysql.update("UPDATE Teamspeak SET RANG= '" + ID + "' WHERE UUID= '" + "'");
    }

    public static void updateType(String NAME, int i) {
        Lobby.mysql.update("UPDATE TeamSpeak SET TYPE= '" + i + "' WHERE NAME='" + NAME + "'");
    }

    public static Integer getType(String uuid) {
        int i = 0;
        try {
            ResultSet rs = Lobby.mysql.getResult("SELECT * FROM TeamSpeak WHERE UUID= '" + uuid + "'");
            if ((!rs.next()) || (Integer.valueOf(rs.getInt("TYPE")) == null));
            i = rs.getInt("TYPE");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
